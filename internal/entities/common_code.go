package entities

import "time"

type CommonCode struct {
	PrdctID  int `gorm:"primaryKey"`
	CodeType string
	CmCode   string
	CdDesc   string
	DelFlg   bool
	ModTime  time.Time
	ModID    int
	CreTime  time.Time
	CreID    int
}

// prdct_id  int not null,
// code_type varchar,
// cm_code   varchar,
// cd_desc   varchar,
// del_flg   bool,
// mod_time  timestamp,
// mod_id    int,
// cre_time  timestamp,
// cre_id    int
