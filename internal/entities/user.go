package entities

import "time"

type User struct {
	UserID    string `gorm:"primaryKey;autoIncrement:false"`
	Username  string `gorm:"unique;->;<-:create"`
	Password  string
	FName     string
	LName     string
	CustType  string
	AddrCity  string
	AddrLine1 string
	AddrLine2 string
	DelFlg    bool
	ModTime   time.Time
	ModID     int
	CreTime   time.Time
	CreID     int
}

// user_id    varchar not null // primary key // unique,
// f_name     varchar,
// l_name     varchar,
// cust_type  varchar,
// addr_city  varchar,
// addr_line1 varchar,
// addr_line2 varchar,
// del_flg    bool,
// mod_time   timestamp,
// mod_id     int,
// cre_time   timestamp,
// cre_id     int
