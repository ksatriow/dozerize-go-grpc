package service

import (
	"assignment-20220907-bima/internal/entities"
	"assignment-20220907-bima/pkg/v1/jwt"
	userpb "assignment-20220907-bima/proto/v1/users"
	"context"
)

type UserService interface {
	AddUser(context.Context, *userpb.RequestAdd) (*entities.User, error)
	EditUser(context.Context, *userpb.RequestEdit) (*entities.User, error)
	DeleteUser(context.Context, *userpb.RequestUserID) (bool, error)
	ShowAll(context.Context) ([]*entities.User, error)
	ShowDetail(context.Context, *userpb.RequestUserID) (*entities.User, error)

	Login(context.Context, *userpb.RequestLogin) (*jwt.Token, error)
	IsUsernameExists(string) (bool, error)
}
