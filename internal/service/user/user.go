package service

import (
	"assignment-20220907-bima/configs"
	"assignment-20220907-bima/internal/entities"
	repository "assignment-20220907-bima/internal/repository/user"
	"assignment-20220907-bima/pkg/v1/jwt"
	userpb "assignment-20220907-bima/proto/v1/users"
	"context"
	"errors"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

// impl UserService
type userService struct {
	config *configs.Configs
	logger *logrus.Logger

	repo repository.UserRepository
}

func NewUserService(config *configs.Configs, logger *logrus.Logger, repo repository.UserRepository) UserService {
	return &userService{
		config: config,
		logger: logger,
		repo:   repo,
	}
}

func (us *userService) Login(ctx context.Context, req *userpb.RequestLogin) (*jwt.Token, error) {
	username, password := req.Username, req.Password
	user, err := us.repo.GetByUsername(username)

	if err != nil {
		return nil, err
	}

	if user.UserID == "" {
		return nil, errors.New("Invalid credentials")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return nil, errors.New("Invalid credentials")
	}

	// create *jwt.Token
	userPayload, err := jwt.EToPayload(*user)
	if err != nil {
		return nil, err
	}

	authToken, err := jwt.GenerateToken(us.config.Config, userPayload)
	if err != nil {
		return nil, err
	}

	return authToken, nil
}

func (us *userService) AddUser(ctx context.Context, req *userpb.RequestAdd) (*entities.User, error) {
	// TODO: validation
	username, password := req.Username, req.Password

	// check if username is taken
	usernameTaken, err := us.IsUsernameExists(username)
	if err != nil {
		return nil, err
	}

	if usernameTaken {
		return nil, errors.New("Username is already used.")
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	user := &entities.User{
		UserID:    req.UserId,
		Username:  req.Username,
		Password:  string(passwordHash),
		FName:     req.FName,
		LName:     req.FName,
		CustType:  req.CustType,
		AddrCity:  req.AddrCity,
		AddrLine1: req.AddrLine1,
		AddrLine2: req.AddrLine2,
		CreTime:   time.Now(),
		CreID:     0, // TODO: mungkin setelah ditambah otentikasi
	}

	insertedUser, err := us.repo.Create(user)
	if err != nil {
		return nil, err
	}

	return insertedUser, nil
}

func (us *userService) EditUser(ctx context.Context, req *userpb.RequestEdit) (*entities.User, error) {
	user, err := us.repo.GetById(req.UserId)
	if err != nil {
		return nil, err
	}

	if user.UserID == "" {
		return nil, errors.New("user not found")
	}

	// change values
	user.FName = req.FName
	user.LName = req.LName
	user.CustType = req.CustType
	user.AddrCity = req.AddrCity
	user.AddrLine1 = req.AddrLine1
	user.AddrLine2 = req.AddrLine2
	user.ModTime = time.Now()
	user.ModID = 0 // TODO: mungkin setelah ditambah otentikasi

	// Jika password diisi artinya ganti password
	if req.Password != "" {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}

		user.Password = string(passwordHash)
	}

	updatedUser, err := us.repo.Update(user)
	if err != nil {
		return nil, err
	}

	return updatedUser, nil
}

func (us *userService) DeleteUser(ctx context.Context, req *userpb.RequestUserID) (bool, error) {
	isDeleted, err := us.repo.Delete(req.UserId)
	if err != nil {
		return false, err
	}

	return isDeleted, nil
}

func (us *userService) ShowAll(ctx context.Context) ([]*entities.User, error) {
	users, err := us.repo.GetAll()
	if err != nil {
		return []*entities.User{}, err
	}

	return users, nil
}

func (us *userService) ShowDetail(ctx context.Context, req *userpb.RequestUserID) (*entities.User, error) {
	user, err := us.repo.GetById(req.UserId)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (us *userService) IsUsernameExists(username string) (bool, error) {
	user, err := us.repo.GetByUsername(username)
	if err != nil {
		return false, err
	}

	if user.UserID == "" {
		return false, nil
	}

	return true, nil
}
