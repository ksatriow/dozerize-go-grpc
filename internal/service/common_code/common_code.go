package service

import (
	"assignment-20220907-bima/internal/entities"
	repository "assignment-20220907-bima/internal/repository/common_code"
	ccpb "assignment-20220907-bima/proto/v1/common_code"
	"context"
	"errors"
	"time"
)

// impl CommonCodeService
type commonCode struct {
	repo repository.CommonCodeRepository
}

func NewCommonCodeService(repo repository.CommonCodeRepository) CommonCodeService {
	return &commonCode{repo: repo}
}

func (ccs *commonCode) AddCC(ctx context.Context, req *ccpb.RequestAdd) (*entities.CommonCode, error) {
	// TODO: validate input dari user

	cc := &entities.CommonCode{
		CodeType: req.CodeType,
		CmCode:   req.CmCode,
		CdDesc:   req.CdDesc,
		DelFlg:   false,
		CreTime:  time.Now(),
		CreID:    0, // TODO: setelah implement authentication
	}

	insertedCC, err := ccs.repo.Add(cc)
	if err != nil {
		return nil, err
	}

	return insertedCC, nil
}

func (ccs *commonCode) EditCC(ctx context.Context, req *ccpb.RequestEdit) (*entities.CommonCode, error) {
	// TODO: validate input dari user

	cc, err := ccs.repo.ShowDetail(int(req.PrdctId))
	if err != nil {
		return nil, err
	}

	if cc.PrdctID == 0 {
		return nil, errors.New("common code not found")
	}

	// update values
	cc.CodeType = req.CodeType
	cc.CmCode = req.CmCode
	cc.CdDesc = req.CdDesc
	cc.ModTime = time.Now()
	cc.ModID = 0 // TODO: saat auth sudah selesai

	updatedCC, err := ccs.repo.Edit(cc)
	if err != nil {
		return nil, err
	}

	return updatedCC, nil
}

func (ccs *commonCode) DeleteCC(ctx context.Context, req *ccpb.RequestCommonCodePrdctID) (bool, error) {
	isDeleted, err := ccs.repo.Delete(int(req.PrdctId))
	if err != nil {
		return false, err
	}

	return isDeleted, nil
}

func (ccs *commonCode) ShowAll(ctx context.Context) ([]*entities.CommonCode, error) {
	ccList, err := ccs.repo.ShowAll()
	if err != nil {
		return []*entities.CommonCode{}, err
	}

	return ccList, nil
}

func (ccs *commonCode) ShowDetail(ctx context.Context, req *ccpb.RequestCommonCodePrdctID) (*entities.CommonCode, error) {
	cc, err := ccs.repo.ShowDetail(int(req.PrdctId))
	if err != nil {
		return nil, err
	}

	return cc, nil
}
