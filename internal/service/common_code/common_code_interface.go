package service

import (
	"assignment-20220907-bima/internal/entities"
	ccpb "assignment-20220907-bima/proto/v1/common_code"
	"context"
)

type CommonCodeService interface {
	AddCC(context.Context, *ccpb.RequestAdd) (*entities.CommonCode, error)
	EditCC(context.Context, *ccpb.RequestEdit) (*entities.CommonCode, error)
	DeleteCC(context.Context, *ccpb.RequestCommonCodePrdctID) (bool, error)
	ShowAll(context.Context) ([]*entities.CommonCode, error)
	ShowDetail(context.Context, *ccpb.RequestCommonCodePrdctID) (*entities.CommonCode, error)
}
