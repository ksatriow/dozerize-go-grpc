package repository

import "assignment-20220907-bima/internal/entities"

type UserRepository interface {
	Create(user *entities.User) (*entities.User, error)
	Update(user *entities.User) (*entities.User, error)
	Delete(user_id string) (bool, error)
	GetAll() ([]*entities.User, error)
	GetById(user_id string) (*entities.User, error)
	GetByUsername(username string) (*entities.User, error)
}
