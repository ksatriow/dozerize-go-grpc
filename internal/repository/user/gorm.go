package repository

import (
	"assignment-20220907-bima/configs"
	"assignment-20220907-bima/internal/entities"
	"errors"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type userRepositoryGORM struct {
	config *configs.Configs
	logger *logrus.Logger

	db *gorm.DB
}

func NewUserRepositoryGORM(config *configs.Configs, logger *logrus.Logger, db *gorm.DB) UserRepository {
	return &userRepositoryGORM{
		config: config,
		logger: logger,
		db:     db,
	}
}

func (r *userRepositoryGORM) Create(user *entities.User) (*entities.User, error) {
	err := r.db.Create(&user).Error
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *userRepositoryGORM) Update(user *entities.User) (*entities.User, error) {
	err := r.db.Save(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepositoryGORM) Delete(user_id string) (bool, error) {
	user, err := r.GetById(user_id)
	if err != nil {
		return false, err
	}

	// check apakah user found
	if user.UserID == "" {
		return false, errors.New("user not found")
	}

	err = r.db.Where("user_id = ?", user_id).Delete(&user).Error
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *userRepositoryGORM) GetAll() ([]*entities.User, error) {
	var users []*entities.User

	err := r.db.Where("del_flg IS NOT TRUE").Find(&users).Error
	if err != nil {
		return users, err
	}

	return users, nil
}

func (r *userRepositoryGORM) GetById(user_id string) (*entities.User, error) {
	var user *entities.User

	err := r.db.Where("user_id = ?", user_id).Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepositoryGORM) GetByUsername(username string) (*entities.User, error) {
	var user *entities.User

	err := r.db.Where("username = ?", username).Find(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}
