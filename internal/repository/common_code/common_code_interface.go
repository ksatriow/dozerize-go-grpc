package repository

import "assignment-20220907-bima/internal/entities"

type CommonCodeRepository interface {
	Add(cc *entities.CommonCode) (*entities.CommonCode, error)
	Edit(cc *entities.CommonCode) (*entities.CommonCode, error)
	Delete(prdct_id int) (bool, error)
	ShowAll() ([]*entities.CommonCode, error)
	ShowDetail(prdct_id int) (*entities.CommonCode, error)
}
