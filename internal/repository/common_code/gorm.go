package repository

import (
	"assignment-20220907-bima/internal/entities"
	"errors"

	"gorm.io/gorm"
)

type commonCodeRepository struct {
	db *gorm.DB
}

func NewCommonCodeRepositoryGORM(db *gorm.DB) CommonCodeRepository {
	return &commonCodeRepository{db: db}
}

func (r *commonCodeRepository) Add(cc *entities.CommonCode) (*entities.CommonCode, error) {
	err := r.db.Create(&cc).Error
	if err != nil {
		return nil, err
	}

	return cc, err
}

func (r *commonCodeRepository) Edit(cc *entities.CommonCode) (*entities.CommonCode, error) {
	err := r.db.Save(&cc).Error
	if err != nil {
		return cc, err
	}

	return cc, nil
}

func (r *commonCodeRepository) Delete(prdct_id int) (bool, error) {
	cc, err := r.ShowDetail(prdct_id)

	if err != nil {
		return false, err
	}

	if cc.PrdctID == 0 {
		return false, errors.New("common Code not found.")
	}

	err = r.db.Where("prdct_id = ?", prdct_id).Delete(&cc).Error
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *commonCodeRepository) ShowAll() ([]*entities.CommonCode, error) {
	var ccList []*entities.CommonCode

	err := r.db.Where("del_flg IS NOT TRUE").Find(&ccList).Error
	if err != nil {
		return ccList, err
	}

	return ccList, err
}

func (r *commonCodeRepository) ShowDetail(prdct_id int) (*entities.CommonCode, error) {
	var cc *entities.CommonCode

	err := r.db.Where("prdct_id = ?", prdct_id).Find(&cc).Error
	if err != nil {
		return cc, err
	}

	return cc, nil
}
