package main

import (
	"assignment-20220907-bima/configs"
	"assignment-20220907-bima/router"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"golang.org/x/sync/errgroup"
	"gorm.io/gorm"
)

func main() {
	var err error

	configs, logger, db, err := configs.New()
	if err != nil {
		log.Printf("[SERVER] ERROR %v", err)
		log.Fatal(err)
	}

	// close db connection
	defer func(db *gorm.DB) {
		dbConn, err := db.DB()
		if err != nil {
			log.Fatal("Failed to close database connection.")
		}

		err = dbConn.Close()
		if err != nil {
			log.Fatal("Failed to close database connection.")
		}

		log.Println("Database connection closed.")
	}(db)

	logger.Infof("[SERVER] Environment %s is ready", configs.Config.Env)

	g, _ := errgroup.WithContext(context.Background())

	var servers []*http.Server

	g.Go(func() error {
		signalChannel := make(chan os.Signal, 1)
		signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)

		select {
		case sig := <-signalChannel:
			logger.Infof("receive signal: %s\n", sig)
			for i, s := range servers {
				if err := s.Shutdown(context.Background()); err != nil {
					if err == nil {
						logger.Infof("error shutting down server %d: %v", i, err)
						return err
					}
				}
			}
			os.Exit(1)
		}
		return nil
	})

	g.Go(func() error { return router.NewGRPCServer(configs, logger, db) })
	g.Go(func() error { return router.NewHTTPServer(configs, logger, db) })

	if err := g.Wait(); !router.IgnoreErr(err) {
		log.Printf("[SERVER] ERROR %v", err)
		logger.Fatal(err)
	}

	logger.Infoln("[APP] DONE.")
}
