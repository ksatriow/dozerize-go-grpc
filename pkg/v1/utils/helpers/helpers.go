package helpers

import (
	"assignment-20220907-bima/internal/entities"
	"assignment-20220907-bima/pkg/v1/jwt"
	"context"
	"errors"
)

func GetLoggedInUserId(ctx context.Context) (string, error) {
	loginUser, err := GetLoggedInUser(ctx)

	if err != nil {
		return "", err
	}

	return loginUser.UserID, nil
}

func GetLoggedInUser(ctx context.Context) (*entities.User, error) {
	userdata, ok := ctx.Value("userdata").(jwt.CustomClaims)

	if !ok {
		return &entities.User{}, errors.New("Invalid format claims")
	}

	user, err := jwt.PayloadToE(&userdata.User)
	if err != nil {
		return &entities.User{}, err
	}

	return user, nil
}
