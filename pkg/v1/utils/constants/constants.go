package constants

const (
	EnvProduction = "production"
)

const (
	SuccessCode = `0000`
	SuccesDesc  = `SUCCESS`
)

const (
	Jwt_Token_Expired_Periode   = 60 * 60 // 1 jam
	Jwt_Refresh_Expired_Periode = 86400
)

const (
	Endpoint_Login = `/api.assignment01.v1.users.UsersService/Login`
)
