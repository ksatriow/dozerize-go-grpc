package middleware

import (
	"assignment-20220907-bima/configs"
	"assignment-20220907-bima/pkg/v1/jwt"
	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	logpkg "assignment-20220907-bima/pkg/v1/utils/logger"
	"context"
	"encoding/json"
	"strconv"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"gorm.io/gorm"
)

func NewAuthInterceptor(configs *configs.Configs, logger *logrus.Logger, db *gorm.DB) funcInterceptor {
	return func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		var authCtx context.Context = nil

		if !(info.FullMethod == constants.Endpoint_Login) {
			headers, ok := metadata.FromIncomingContext(ctx)
			if !ok {
				return nil, errors.New("error get context")
			}

			if len(headers.Get("Authorization")) < 1 || headers.Get("Authorization")[0] == "" {
				return nil, errors.FormatError(codes.InvalidArgument, &errors.Response{
					Code: "1000",
					Msg:  "header Authorization is empty",
				})
			}

			// Verifying token
			claims, err := jwt.ClaimToken(configs.Config, headers.Get("Authorization")[0], false)
			if err != nil {
				return nil, errors.FormatError(codes.Unauthenticated, &errors.Response{
					Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Unauthenticated)),
					Msg:  err.Error(),
				})
			}

			// simpan data claim dengan tipe data struct CustomClaims
			jsonClaim, err := json.Marshal(claims)
			if err != nil {
				return nil, errors.FormatError(codes.Internal, &errors.Response{
					Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
					Msg:  err.Error(),
				})
			}

			jwtClaim := jwt.CustomClaims{}
			if err := json.Unmarshal(jsonClaim, &jwtClaim); err != nil {
				return nil, errors.FormatError(codes.Internal, &errors.Response{
					Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
					Msg:  err.Error(),
				})
			}

			authCtx = context.WithValue(ctx, "userdata", jwtClaim)
		}

		// store request log
		err := logpkg.StoreRestRequest(ctx, req, info, "")
		if err != nil {
			return nil, err
		}

		if authCtx != nil {
			return handler(authCtx, req)
		}

		return handler(ctx, req)
	}
}
