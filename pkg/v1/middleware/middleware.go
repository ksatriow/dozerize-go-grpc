package middleware

import (
	"context"

	"google.golang.org/grpc"
)

type funcInterceptor func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error)
