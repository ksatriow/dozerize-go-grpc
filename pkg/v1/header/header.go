package header

import (
	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/logger"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/golang/protobuf/proto"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
)

const (
	CHANNEL_CONTEXT = `Channel-Context`
)

type ChannelCtx struct {
	Success        bool   `json:"success,omitempty"`
	Code           string `json:"code,omitempty"`
	Desc           string `json:"desc,omitempty"`
	AdditionalInfo string `json:"additionalInfo,omitempty"`
}

type Message struct {
	MessageCode string
	MessageDesc string
}

func CreateSuccessChannelContext(numrec int) ([]byte, error) {
	chnCTx, err := json.Marshal(ChannelCtx{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
	})
	if err != nil {
		return nil, err
	}

	return chnCTx, nil
}

func HttpResponseModifier(ctx context.Context, w http.ResponseWriter, p proto.Message) error {
	var chnCtx string

	// store response log
	err := logger.StoreRestResponse(ctx, w, p)
	if err != nil {
		return err
	}

	md, ok := runtime.ServerMetadataFromContext(ctx)
	if !ok {
		return nil
	}

	chnCtxs := md.HeaderMD.Get(CHANNEL_CONTEXT)
	if len(chnCtxs) > 0 {
		chnCtx = chnCtxs[0]
	}

	// set header
	Set(w, chnCtx)

	delete(w.Header(), "Grpc-Metadata-User-Id")
	delete(w.Header(), "Grpc-Metadata-Channel-Context")

	return nil
}

// Header for http response
func Set(w http.ResponseWriter, data string) {
	w.Header().Set(CHANNEL_CONTEXT, data)
	w.Header().Set("Access-Control-Expose-Headers", fmt.Sprintf("%s,Content-Disposition", CHANNEL_CONTEXT))

	// w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
	// w.Header().Set("X-Powered-By", "Undertow/1")
	// w.Header().Set("Access-Control-Allow-Headers", "X-Requested-With,content-type,Accept,Authorization,OCH_CATEGORY_ID,OCH_RANDOM_KEY,OCH_KEY_ID")
	// w.Header().Set("Server", "PINUAT")
	// w.Header().Set("Pragma", "no-cache")
	// w.Header().Set("Access-Control-Allow-Origin", "*")
	// w.Header().Set("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS")
	// w.Header().Set("Connection", "close")
}
