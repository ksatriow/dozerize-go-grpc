package router

import (
	"assignment-20220907-bima/api/v1/common_code"
	"assignment-20220907-bima/api/v1/users"
	"assignment-20220907-bima/configs"
	ccpb "assignment-20220907-bima/proto/v1/common_code"
	userpb "assignment-20220907-bima/proto/v1/users"
	"fmt"
	"log"
	"net"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"gorm.io/gorm"
)

func NewGRPCServer(configs *configs.Configs, logger *logrus.Logger, db *gorm.DB) error {
	listen, err := net.Listen("tcp", fmt.Sprintf(":%v", configs.Config.Server.Grpc.Port))
	if err != nil {
		return err
	}

	// middlewares / interceptor
	// authInterceptor := middleware.NewAuthInterceptor(configs, logger, db)

	// register grpc service server
	grpcServer := grpc.NewServer(
	// grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
	// 	grpc.UnaryServerInterceptor(authInterceptor),
	// ))
	)

	// Register grpc services here....
	userpb.RegisterUsersServiceServer(grpcServer, users.New(configs, logger, db))
	ccpb.RegisterCommonCodeServiceServer(grpcServer, common_code.New(configs, logger, db))

	// add reflection service
	reflection.Register(grpcServer)

	// running gRPC server
	log.Println("[SERVER] GRPC server is running")

	grpcServer.Serve(listen)

	return nil
}
