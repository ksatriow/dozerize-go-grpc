package router

import (
	"assignment-20220907-bima/configs"
	"assignment-20220907-bima/pkg/v1/header"
	"assignment-20220907-bima/pkg/v1/utils/constants"
	userpb "assignment-20220907-bima/proto/v1/users"
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

func NewHTTPServer(configs *configs.Configs, logger *logrus.Logger, db *gorm.DB) error {

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Connect to the GRPC server
	address := fmt.Sprintf("0.0.0.0:%v", configs.Config.Server.Grpc.Port)
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	// Create new grpc-gateway
	// rmux := runtime.NewServeMux()
	rmux := runtime.NewServeMux(runtime.WithForwardResponseOption(header.HttpResponseModifier))

	// register gateway endpoints
	for _, f := range []func(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error{
		// register grpc services handler here...
		userpb.RegisterUsersServiceHandler,
	} {
		if err = f(ctx, rmux, conn); err != nil {
			return err
		}
	}

	mux := http.NewServeMux()
	mux.Handle("/", rmux)

	if configs.Config.Env != constants.EnvProduction {
		CreateSwagger(mux)
	}

	headerOk := handlers.AllowedHeaders([]string{"Accept", "Accept-Language", "Content-Language", "Content-Type", "X-Requested-With", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "Timezone-Offset"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	// running rest http server
	log.Println("[SERVER] REST HTTP server is ready")

	err = http.ListenAndServe(fmt.Sprintf("0.0.0.0:%v", configs.Config.Server.Rest.Port), handlers.CORS(headerOk, originsOk, methodsOk)(mux))
	if err != nil {
		return err
	}

	return nil
}

// CreateSwagger creates the swagger server serve mux
func CreateSwagger(gwmux *http.ServeMux) {
	// register swagger service server
	gwmux.HandleFunc("/api/v1/check/docs.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "swagger/docs.json")
	})

	// load swagger-ui file
	fs := http.FileServer(http.Dir("swagger/swagger-ui"))
	gwmux.Handle("/api/v1/check/docs/", http.StripPrefix("/api/v1/check/docs", fs))
}
