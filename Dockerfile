FROM golang:alpine as builder

RUN apk update && apk add --no-cache git

WORKDIR /app

# /app 
COPY . /app

# go mod
RUN go mod tidy && go mod download && go mod verify

# COPY .env .
RUN go build -o dockerize_grpc /app/main.go

RUN chmod +x ./dockerize_grpc

ENTRYPOINT [ "./dockerize_grpc" ]

FROM alpine:latest AS production

COPY --from=builder /app .

CMD ["./dockerize_grpc"]