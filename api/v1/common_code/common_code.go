package common_code

import (
	"assignment-20220907-bima/configs"
	repository "assignment-20220907-bima/internal/repository/common_code"
	service "assignment-20220907-bima/internal/service/common_code"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type Server struct {
	config    *configs.Configs
	logger    *logrus.Logger
	ccService service.CommonCodeService
}

func New(config *configs.Configs, logger *logrus.Logger, db *gorm.DB) *Server {
	repo := repository.NewCommonCodeRepositoryGORM(db)
	service := service.NewCommonCodeService(repo)

	return &Server{
		config:    config,
		logger:    logger,
		ccService: service,
	}
}
