package common_code

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	ccpb "assignment-20220907-bima/proto/v1/common_code"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) ShowDetail(ctx context.Context, req *ccpb.RequestCommonCodePrdctID) (*ccpb.Response, error) {
	cc, err := s.ccService.ShowDetail(ctx, req)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	if cc.PrdctID == 0 {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  "Common Code not found",
		})
	}

	return &ccpb.Response{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
		Data: &ccpb.CommonCode{
			PrdctId:  int32(cc.PrdctID),
			CodeType: cc.CodeType,
			CmCode:   cc.CmCode,
			CdDesc:   cc.CdDesc,
			DelFlg:   cc.DelFlg,
		},
	}, nil
}
