package common_code

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	ccpb "assignment-20220907-bima/proto/v1/common_code"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) Delete(ctx context.Context, req *ccpb.RequestCommonCodePrdctID) (*ccpb.ResponseDelete, error) {
	_, err := s.ccService.DeleteCC(ctx, req)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	return &ccpb.ResponseDelete{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
	}, nil
}
