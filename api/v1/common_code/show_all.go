package common_code

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	ccpb "assignment-20220907-bima/proto/v1/common_code"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) ShowAll(ctx context.Context, req *empty.Empty) (*ccpb.ResponseList, error) {
	ccList, err := s.ccService.ShowAll(ctx)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	ccPbList := []*ccpb.CommonCode{}
	for _, cc := range ccList {
		ccPbList = append(ccPbList, &ccpb.CommonCode{
			PrdctId:  int32(cc.PrdctID),
			CodeType: cc.CodeType,
			CmCode:   cc.CmCode,
			CdDesc:   cc.CdDesc,
			DelFlg:   cc.DelFlg,
		})
	}

	return &ccpb.ResponseList{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
		Data:    ccPbList,
	}, nil
}
