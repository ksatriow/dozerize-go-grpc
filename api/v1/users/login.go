package users

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	userpb "assignment-20220907-bima/proto/v1/users"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) Login(ctx context.Context, req *userpb.RequestLogin) (*userpb.Response, error) {
	auth, err := s.userService.Login(ctx, req)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	return &userpb.Response{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
		Auth: &userpb.Auth{
			Type:           auth.Type,
			Access:         auth.Access,
			ExpiredPeriode: int32(auth.ExpiredPeriode),
			Refresh:        auth.Refresh,
		},
	}, nil
}
