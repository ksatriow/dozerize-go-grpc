package users

import (
	"assignment-20220907-bima/configs"
	repository "assignment-20220907-bima/internal/repository/user"
	service "assignment-20220907-bima/internal/service/user"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type Server struct {
	config      *configs.Configs
	logger      *logrus.Logger
	userService service.UserService
}

func New(config *configs.Configs, logger *logrus.Logger, db *gorm.DB) *Server {
	// Create repo dan service yang dibutuhkan
	repo := repository.NewUserRepositoryGORM(config, logger, db)
	service := service.NewUserService(config, logger, repo)

	return &Server{
		config:      config,
		logger:      logger,
		userService: service,
	}
}
