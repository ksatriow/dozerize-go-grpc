package users

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	userpb "assignment-20220907-bima/proto/v1/users"

	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) Edit(ctx context.Context, req *userpb.RequestEdit) (*userpb.Response, error) {
	user, err := s.userService.EditUser(ctx, req)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	return &userpb.Response{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
		Data: &userpb.User{
			UserId:    user.UserID,
			FName:     user.FName,
			LName:     user.LName,
			CustType:  user.CustType,
			AddrCity:  user.AddrCity,
			AddrLine1: user.AddrLine1,
			AddrLine2: user.AddrLine2,
			DelFlg:    user.DelFlg,
			CreTime: &timestamp.Timestamp{
				Seconds: user.CreTime.Unix(),
			},
			ModTime: &timestamp.Timestamp{
				Seconds: user.ModTime.Unix(),
			},
		},
	}, nil
}
