package users

import (
	"context"
	"strconv"

	"assignment-20220907-bima/pkg/v1/utils/constants"
	"assignment-20220907-bima/pkg/v1/utils/errors"
	userpb "assignment-20220907-bima/proto/v1/users"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
)

func (s *Server) Delete(ctx context.Context, req *userpb.RequestUserID) (*userpb.Response, error) {
	isDeleted, err := s.userService.DeleteUser(ctx, req)

	if err != nil {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  err.Error(),
		})
	}

	if !isDeleted {
		return nil, errors.FormatError(codes.Internal, &errors.Response{
			Code: strconv.Itoa(runtime.HTTPStatusFromCode(codes.Internal)),
			Msg:  "Cannot delete common code",
		})
	}

	return &userpb.Response{
		Success: true,
		Code:    constants.SuccessCode,
		Desc:    constants.SuccesDesc,
	}, nil
}
