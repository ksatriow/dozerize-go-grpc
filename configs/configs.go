package configs

import (
	"assignment-20220907-bima/internal/entities"
	"assignment-20220907-bima/pkg/v1/config"
	"fmt"
	"log"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// bundle/wrap another service access here
type Configs struct {
	Config *config.Config
}

func New() (*Configs, *logrus.Logger, *gorm.DB, error) {
	config, err := config.New()

	if err != nil {
		return nil, nil, nil, err
	}

	// force all writes to regular log to logger
	logger := logrus.New()
	log.SetOutput(logger.Writer()) // TODO: ?
	log.SetFlags(0)                // TODO: ?

	logger.Formatter = &logrus.TextFormatter{
		ForceColors:   true,
		ForceQuote:    true,
		FullTimestamp: true,
	}

	// TODO: pg letter

	// Gorm
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable TimeZone=Asia/Jakarta",
		config.DB.Host,
		config.DB.User,
		config.DB.Pass,
		config.DB.DbName,
		config.DB.Port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, nil, nil, err
	}

	logger.Info("[CONFIG] Connected to Database")

	// Auto Migrate
	db.AutoMigrate(&entities.User{}, &entities.CommonCode{})

	logger.Info("[CONFIG] Setup complete")

	return &Configs{
		Config: config,
	}, logger, db, nil
}
